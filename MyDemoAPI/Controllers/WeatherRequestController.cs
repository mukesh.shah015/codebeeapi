﻿using BasicAuthentication;
using MyDemoAPI.DAL;
using MyDemoAPI.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using static MyDemoAPI.Models.Main;

namespace MyDemoAPI.Controllers
{
    public class WeatherRequestController : ApiController
    {
        private ClientContext db = new ClientContext();

        [BasicAuthentication]
        public dynamic GetWeatherReport(string latitude, string longitude)
        {
            try
            {
                if (string.IsNullOrEmpty(latitude) && string.IsNullOrEmpty(longitude))
                {
                    return new ApiResponse { Code = StatusCodes.Status200OK, Message = "Please supply latitude and longitude parameters inorder to continue" };

                }
                WebClient webClient = new WebClient();
                webClient.Headers.Add("user-agent", "Only a test!");
                var companyname = "";
                
                string hostName = Dns.GetHostName();
                

                string IP = Dns.GetHostByName(hostName).AddressList[0].ToString();
                var totalRequest = 0; 
               
                var userId = Convert.ToString(GlobalArray.UserId);
                var uId = 0;

                if (!string.IsNullOrEmpty(userId))
                {
                    uId = Convert.ToInt32(userId);
                }
                var list = db.RequestLog.ToList();

                if (uId > 0)
                {
                    companyname = db.Client.Where(x => x.Id == uId).FirstOrDefault().CompanyName;
                     totalRequest = list.Where(x => Convert.ToDateTime(x.RequestTime).AddHours(1) >= DateTime.Now && x.ClientId == uId).Count();

                    if (totalRequest >= 3)
                    {
                        return new ApiResponse { Code = StatusCodes.Status200OK, Message = "Your hourly request has been exceed" };

                    }
                }
                else
                {
                    totalRequest = list.Where(x => Convert.ToDateTime(x.RequestTime).AddHours(1) >= DateTime.Now).Count();
                    if (totalRequest >= 1)
                    {
                        return new ApiResponse { Code = StatusCodes.Status200OK, Message = "Your hourly request has been exceed" };

                    }
                }
                var rLog = new RequestLog();
                rLog.IP = IP;
                rLog.RequestTime = DateTime.Now;
                if (uId > 0)
                {
                    rLog.ClientId = uId;
                }
                db.RequestLog.Add(rLog);
                db.SaveChanges();
                var url = "https://fcc-weather-api.glitch.me/api/current?";
                var param = "lat=" + latitude + "&lon=" + longitude + "";
                url = url + param;

                string content = webClient.DownloadString(url);
                dynamic data = JObject.Parse(content);
                var wInfo = data.weather[0].description;
                var temp = data.main.temp;
               
                var companyName = "";
                String timeStamp = GetTimestamp(DateTime.Now);

                return new Report { TimeStamp = timeStamp, CompanyName = companyname, WeatherInfo = Convert.ToString(wInfo) , RequestIP =IP,CoOrdinates = "lat : " + latitude +" - long : " + longitude , Temperature = Convert.ToString(temp)};
               
            }
            catch (Exception ex)
            {

                return new ApiResponse { Code = StatusCodes.Status200OK, Message = ex.Message };
            }


        }
        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
}

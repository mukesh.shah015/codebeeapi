﻿using MyDemoAPI.DAL;
using MyDemoAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using static MyDemoAPI.Models.Main;

namespace BasicAuthentication
{
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization != null)
            {
                var authToken = actionContext.Request.Headers
                    .Authorization.Parameter;

                // decoding authToken we get decode value in 'Username:Password' format
                var decodeauthToken = System.Text.Encoding.UTF8.GetString(
                    Convert.FromBase64String(authToken));

                // spliting decodeauthToken using ':' 
                var arrUserNameandPassword = decodeauthToken.Split(':');

                // at 0th postion of array we get username and at 1st we get password
                if (IsAuthorizedUser(arrUserNameandPassword[0], arrUserNameandPassword[1]))
                {
                    // setting current principle
                    Thread.CurrentPrincipal = new GenericPrincipal(
                    new GenericIdentity(arrUserNameandPassword[0]), null);
                }
                else
                {
                    actionContext.Response = actionContext.Request
                    .CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            else
            {
                GlobalArray.UserId = 0;
                GlobalArray.HasError = true;
                //actionContext.Response = actionContext.Request
                // .CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        public static dynamic IsAuthorizedUser(string Username, string Password)
        {
            var db = new ClientContext();
            Password = GlobalRepo.Encrypt(Password);
            var isValidUser = db.Client.Where(x => x.UserName.ToLower() == Username.ToLower() && x.Password == Password).Any();
            var cliendId = 0;
          
            if (isValidUser == true)
            {
                cliendId = db.Client.Where(x => x.UserName == Username && x.Password == Password).First().Id;
                GlobalArray.UserId = cliendId;
            }
            GlobalArray.UserId = cliendId;
            //var  Username == "bhushan" && Password == "demo";
            // In this method we can handle our database logic here...
            //return new ApiResponse { success = isValidUser };

            return isValidUser;
        }
    }
}
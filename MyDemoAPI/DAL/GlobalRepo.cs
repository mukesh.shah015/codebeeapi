﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MyDemoAPI.DAL
{
    public class GlobalRepo
    {
        public static string Encrypt(string ToBeEncrypted)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            string Password = "@MukeshDemoProject@";
            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(ToBeEncrypted);
            byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);
            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(PlainText, 0, PlainText.Length);
            cryptoStream.FlushFinalBlock();
            byte[] CipherBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string EncryptedData = Convert.ToBase64String(CipherBytes);
            return EncryptedData;
        }
        public static string Decrypt(string ToBeDecrypted)
        {
            if (ToBeDecrypted != null)
                ToBeDecrypted = ToBeDecrypted.Replace(' ', '+');
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            string Password = "@MukeshDemoProject@";
            string DecryptedData;
            try
            {
                byte[] EncryptedData = Convert.FromBase64String(ToBeDecrypted);
                byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());
                PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);
                ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
                MemoryStream memoryStream = new MemoryStream(EncryptedData);
                CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);
                byte[] PlainText = new byte[EncryptedData.Length];
                int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);
                memoryStream.Close();
                cryptoStream.Close();
                DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);
            }
            catch (Exception ex)
            {
                //DecryptedData = ToBeDecrypted;
                DecryptedData = "";
                     

            }
            return DecryptedData;
        }
        public static string EntryDateType(string Date)
        {
            string[] split;
            if (Date != "  /  /")
            {
                if (Date != null && Date != string.Empty)
                {
                    split = Date.Split(new char[] { '/', ' ', '.' });
                    //if (split[0].Length == 1)
                    //    split[0] = '0' + split[0];
                    Date = Convert.ToString(split[2].ToString() + "/" + split[1].ToString() + "/" + split[0].ToString());
                }
                else
                {
                    Date = null;
                }
            }
            else
            {
                Date = null;
            }
            return Date;
        }
    }
}
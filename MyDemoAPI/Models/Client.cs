﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDemoAPI.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string ZipCode { get; set; }
    }
}
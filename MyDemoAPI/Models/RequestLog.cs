﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDemoAPI.Models
{
    public class RequestLog
    {
        public int Id { get; set; }
        public DateTime RequestTime { get; set; }
        public int? ClientId { get; set; }
        public string IP { get; set; }
        public virtual Client Client { get; set; }
    }
}